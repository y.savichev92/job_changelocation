/* global QUnit */

sap.ui.require(["zlocationmanagementv0/1/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
