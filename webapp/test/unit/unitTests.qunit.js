/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"z_location_management_v0.1/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
