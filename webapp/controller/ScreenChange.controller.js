// import {aFilterIm} from './MainView.controller';
sap.ui.define(
    [
        "sap/ui/core/mvc/Controller",
        'sap/ui/core/library',
        'sap/m/MessageToast',
        '/sap/ui/core/Core',
        "sap/ui/model/json/JSONModel",
        'sap/ui/core/routing/History',
        "sap/m/MessageBox"
    ],
    function(BaseController, coreLibrary, MessageToast, Core, JSONModel, History, MessageBox) {
        "use strict";

        var ValueState = coreLibrary.ValueState;

        var vAufnr, vQuant, vOldCode;

        var vWerks, vLgort, vStor, vRack, vCell, vFloor, vType;

        var oSelectWerks, oSelectLgort, oSelectStor, oSelectRack, oSelectCell, oSelectFloor, oSelectType;
        var oBindingLgort, oBindingStor, oBindingRack, oBindingCell, oBindingFloor, oBindingNaming;
        var sKeyWerks, sKeylgort, sKeyStor, sKeyRack, sKeyCell, sKeyFloor, sKeyType, sKeyAufnr;
        var oFilterWerks, oFilterLgort, oFilterStor, oFilterRack, oFilterCell, oFilterFloor, oFilterType;

        var i18n;

        return BaseController.extend("zlocationmanagementv0.1.controller.ScreenChange",
        {
            onInit() {
            //Привязываемся к событию навигации на это окно
            var routerX = sap.ui.core.UIComponent.getRouterFor(this);
            routerX.getRoute("ScreenChange").attachMatched(this.onRouteMatched, this);
            },

            // Инициализация страницы
            onRouteMatched: function(oCaller) {

                i18n = this.getView().getModel("i18n").getResourceBundle();

                var oView = this.getView();

                var tableListBox = [ { Type: "Пример",
                                    Num: "01" },
                                { Type: "S",
                                    Num: "02" },
                                { Type: "Поехали",
                                    Num: "03"
                                }];

                oView.setModel(new JSONModel(tableListBox), "tabs");

                this.getValueScreen();

                vWerks.setText(i18n.getText("valueOfNoneValue"));
                vLgort.setText(i18n.getText("valueOfNoneValue"));
                vStor.setText(i18n.getText("valueOfNoneValue"));
                vRack.setText(i18n.getText("valueOfNoneValue"));
                vCell.setText(i18n.getText("valueOfNoneValue"));
                vFloor.setText(i18n.getText("valueOfNoneValue"));
                vType.setText(i18n.getText("valueOfNoneValue"));

                var oArgs = oCaller.getParameter("arguments");

                if(!oArgs){
                    return;
                }

                var oQuery = oArgs["?query"];

                if(!oQuery){
                    return;
                }
                if(oQuery.Aufnr)
                {
                  vAufnr = oQuery.Aufnr;

                  // Изменение Титульника
                  var sTitle = i18n.getText("titChangeMPL") + ' ' + oQuery.Aufnr;
                  this.getView().byId('page').setTitle(sTitle);
                }

                if(oQuery.Werks)
                {
                  vWerks.setText(oQuery.Werks);
                }

                if(oQuery.Lgort)
                {
                  vLgort.setText(oQuery.Lgort);
                }

                if(oQuery.Stor)
                {
                  vStor.setText(oQuery.Stor);
                }

                if(oQuery.Rack)
                {
                  vRack.setText(oQuery.Rack);
                }

                if(oQuery.Cell)
                {
                  vCell.setText(oQuery.Cell);
                }

                if(oQuery.Floor)
                {
                  vFloor.setText(oQuery.Floor);
                }

                if(oQuery.Quant)
                {
                  vQuant = oQuery.Quant;
                }

                if(oQuery.Type)
                {
                  vType.setText(oQuery.Type);
                }

                if(oQuery.Code)
                {
                  vOldCode = oQuery.Code;
                }

                oSelectWerks.setSelectedKey("");
                oSelectLgort.setSelectedKey("");
                oSelectStor.setSelectedKey("");
                oSelectRack.setSelectedKey("");
                oSelectCell.setSelectedKey("");
                oSelectFloor.setSelectedKey("");
                oSelectType.setSelectedKey("");

                oSelectWerks.setValueState(ValueState.Error);
                oSelectLgort.setValueState(ValueState.Error);
                oSelectStor.setValueState(ValueState.Error);
                oSelectRack.setValueState(ValueState.Error);
                oSelectCell.setValueState(ValueState.Error);
                oSelectFloor.setValueState(ValueState.Error);
                oSelectType.setValueState(ValueState.Error);
            },

            getValueScreen: function()
            {
                // Обращение к объекту по ID
                vWerks = this.getView().byId('textWerks');
                vLgort = this.getView().byId('textLgort');
                vStor = this.getView().byId('textStor');
                vRack = this.getView().byId('textRack');
                vCell = this.getView().byId('textCell');
                vFloor = this.getView().byId('textFloor');
                vType = this.getView().byId('textType');

                oSelectWerks = this.getView().byId('werksInput');
                oSelectLgort = this.getView().byId('lgortInput');
                oSelectStor = this.getView().byId('storInput');
                oSelectRack = this.getView().byId('rackInput');
                oSelectCell = this.getView().byId('cellInput');
                oSelectFloor = this.getView().byId('floorInput');
                oSelectType = this.getView().byId('namingInput');
            },

            getSelKey: function()
            {
                this.getValueScreen();

                // Получение значения указание в поле на экране
                sKeyWerks = oSelectWerks.getSelectedKey();
                sKeylgort = oSelectLgort.getSelectedKey();
                sKeyStor = oSelectStor.getSelectedKey();
                sKeyRack = oSelectRack.getSelectedKey();
                sKeyCell = oSelectCell.getSelectedKey();
                sKeyFloor = oSelectFloor.getSelectedKey();
                sKeyType = oSelectType.getSelectedKey();
            },

            setDataFromDropdown: function()
            {
                this.getSelKey();
                // Получение через обект экрана Сущность сервера
                oBindingLgort = oSelectLgort.getBinding("items");
                oBindingStor = oSelectStor.getBinding("items");
                oBindingRack = oSelectRack.getBinding("items");
                oBindingCell = oSelectCell.getBinding("items");
                oBindingFloor = oSelectFloor.getBinding("items");
                oBindingNaming = oSelectType.getBinding("items");

                oFilterWerks = new sap.ui.model.Filter(
                {
                    path: 'Werks',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeyWerks
                });
                oFilterLgort = new sap.ui.model.Filter(
                {
                    path: 'Lgort',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeylgort
                });
                oFilterStor = new sap.ui.model.Filter(
                {
                    path: 'Stor',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeyStor
                });
                oFilterRack = new sap.ui.model.Filter(
                {
                    path: 'Rack',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeyRack
                });
                oFilterCell = new sap.ui.model.Filter(
                {
                    path: 'Cell',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeyCell
                });
                oFilterFloor = new sap.ui.model.Filter(
                {
                    path: 'Floor',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeyFloor
                });
                oFilterType = new sap.ui.model.Filter(
                {
                    path: 'Name',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeyType
                });
            },

            onChangeWerks: function()
            {
                oSelectLgort.setSelectedKey("");
                oSelectStor.setSelectedKey("");
                oSelectRack.setSelectedKey("");
                oSelectCell.setSelectedKey("");
                oSelectFloor.setSelectedKey("");

                this.setDataFromDropdown();

                if (!sKeyWerks) {
                    oSelectWerks.setValueState(ValueState.Error);
                } else {
                    oSelectWerks.setValueState(ValueState.Success);
                }
                var aFilter = [];

                aFilter.push(oFilterWerks);
                oBindingLgort.filter(aFilter);

                oSelectLgort.setSelectedKey("");

                aFilter.push(oFilterLgort, oFilterStor, oFilterRack,  oFilterCell, oFilterFloor, oFilterType)
                oBindingStor.filter(aFilter);
                oBindingRack.filter(aFilter);
                oBindingCell.filter(aFilter);
                oBindingFloor.filter(aFilter);
                // oBindingNaming.filter(aFilter);

                oSelectLgort.setValueState(ValueState.Error);
                oSelectStor.setValueState(ValueState.Error);
                oSelectRack.setValueState(ValueState.Error);
                oSelectCell.setValueState(ValueState.Error);
                oSelectFloor.setValueState(ValueState.Error);
            },

            onChangeLgort: function()
            {
                oSelectStor.setSelectedKey("");
                oSelectRack.setSelectedKey("");
                oSelectCell.setSelectedKey("");
                oSelectFloor.setSelectedKey("");

                this.setDataFromDropdown();

                if (!sKeyWerks) {
                    oSelectWerks.setValueState(ValueState.Error);
                } else {
                    oSelectWerks.setValueState(ValueState.Success);
                }
                if (!sKeylgort) {
                    oSelectLgort.setValueState(ValueState.Error);
                } else {
                    oSelectLgort.setValueState(ValueState.Success);
                }

                var aFilter = [];

                aFilter.push(oFilterWerks, oFilterLgort, oFilterStor, oFilterRack,  oFilterCell, oFilterFloor, oFilterType)
                oBindingStor.filter(aFilter);
                oBindingRack.filter(aFilter);
                oBindingCell.filter(aFilter);
                oBindingFloor.filter(aFilter);
                // oBindingNaming.filter(aFilter);

                oSelectStor.setValueState(ValueState.Error);
                oSelectRack.setValueState(ValueState.Error);
                oSelectCell.setValueState(ValueState.Error);
                oSelectFloor.setValueState(ValueState.Error);
            },

            onChangeFloor: function()
            {

                oSelectStor.setSelectedKey("");
                oSelectRack.setSelectedKey("");
                oSelectCell.setSelectedKey("");

                this.setDataFromDropdown();

                if (!sKeyWerks) {
                    oSelectWerks.setValueState(ValueState.Error);
                } else {
                    oSelectWerks.setValueState(ValueState.Success);
                }
                if (!sKeylgort) {
                    oSelectLgort.setValueState(ValueState.Error);
                } else {
                    oSelectLgort.setValueState(ValueState.Success);
                }
                if (!sKeyFloor) {
                    oSelectFloor.setValueState(ValueState.Error);
                } else {
                    oSelectFloor.setValueState(ValueState.Success);
                }

                var aFilter = [];

                aFilter.push(oFilterWerks, oFilterLgort, oFilterStor, oFilterRack,  oFilterCell, oFilterFloor, oFilterType)
                oBindingStor.filter(aFilter);
                oBindingRack.filter(aFilter);
                oBindingCell.filter(aFilter);
                // oBindingNaming.filter(aFilter);

                oSelectStor.setValueState(ValueState.Error);
                oSelectRack.setValueState(ValueState.Error);
                oSelectCell.setValueState(ValueState.Error);
            },

            onChangeStor: function()
            {
                oSelectRack.setSelectedKey("");
                oSelectCell.setSelectedKey("");

                this.setDataFromDropdown();

                var aFilter = [];

                if (!sKeyWerks) {
                    oSelectWerks.setValueState(ValueState.Error);
                } else {
                    oSelectWerks.setValueState(ValueState.Success);
                }
                if (!sKeylgort) {
                    oSelectLgort.setValueState(ValueState.Error);
                } else {
                    oSelectLgort.setValueState(ValueState.Success);
                }
                if (!sKeyFloor) {
                    oSelectFloor.setValueState(ValueState.Error);
                } else {
                    oSelectFloor.setValueState(ValueState.Success);
                }
                if (!sKeyStor) {
                    oSelectStor.setValueState(ValueState.Error);
                } else {
                    oSelectStor.setValueState(ValueState.Success);
                }

                aFilter.push(oFilterWerks, oFilterLgort, oFilterStor, oFilterRack,  oFilterCell, oFilterFloor, oFilterType)
                oBindingRack.filter(aFilter);
                oBindingCell.filter(aFilter);
                // oBindingNaming.filter(aFilter);

                oSelectRack.setValueState(ValueState.Error);
                oSelectCell.setValueState(ValueState.Error);
            },

            onChangeRack: function()
            {
                oSelectCell.setSelectedKey("");

                this.setDataFromDropdown();

                if (!sKeyWerks) {
                    oSelectWerks.setValueState(ValueState.Error);
                } else {
                    oSelectWerks.setValueState(ValueState.Success);
                }
                if (!sKeylgort) {
                    oSelectLgort.setValueState(ValueState.Error);
                } else {
                    oSelectLgort.setValueState(ValueState.Success);
                }
                if (!sKeyFloor) {
                    oSelectFloor.setValueState(ValueState.Error);
                } else {
                    oSelectFloor.setValueState(ValueState.Success);
                }
                if (!sKeyStor) {
                    oSelectStor.setValueState(ValueState.Error);
                } else {
                    oSelectStor.setValueState(ValueState.Success);
                }
                if (!sKeyRack) {
                    oSelectRack.setValueState(ValueState.Error);
                } else {
                    oSelectRack.setValueState(ValueState.Success);
                }

                var aFilter = [];

                aFilter.push(oFilterWerks, oFilterLgort, oFilterStor, oFilterRack,  oFilterCell, oFilterFloor, oFilterType)
                oBindingCell.filter(aFilter);
                // oBindingNaming.filter(aFilter);

                oSelectCell.setValueState(ValueState.Error);
            },

            onChangeCell: function()
            {
                this.setDataFromDropdown();

                if (!sKeyWerks) {
                    oSelectWerks.setValueState(ValueState.Error);
                } else {
                    oSelectWerks.setValueState(ValueState.Success);
                }
                if (!sKeylgort) {
                    oSelectLgort.setValueState(ValueState.Error);
                } else {
                    oSelectLgort.setValueState(ValueState.Success);
                }
                if (!sKeyFloor) {
                    oSelectFloor.setValueState(ValueState.Error);
                } else {
                    oSelectFloor.setValueState(ValueState.Success);
                }
                if (!sKeyStor) {
                    oSelectStor.setValueState(ValueState.Error);
                } else {
                    oSelectStor.setValueState(ValueState.Success);
                }
                if (!sKeyRack) {
                    oSelectRack.setValueState(ValueState.Error);
                } else {
                    oSelectRack.setValueState(ValueState.Success);
                }
                if (!sKeyCell) {
                    oSelectCell.setValueState(ValueState.Error);
                } else {
                    oSelectCell.setValueState(ValueState.Success);
                }
                //

                // var aFilter = [];

                // aFilter.push(oFilterWerks, oFilterLgort, oFilterStor, oFilterRack,  oFilterCell, oFilterFloor, oFilterType)
                // oBindingNaming.filter(aFilter);

                // oSelectType.setSelectedKey("");
            },

            onChangeNaming: function()
            {
                this.setDataFromDropdown();

                if (!sKeyType) {
                    oSelectType.setValueState(ValueState.Error);
                } else {
                    oSelectType.setValueState(ValueState.Success);
                }
            },

            getKey(oQuery, flag)
            {
                this.getSelKey();

                if (sKeyWerks)
                {
                    oQuery.Werks = sKeyWerks;
                    oSelectWerks.setValueState(ValueState.Success);
                }
                else
                {
                    flag = 1;
                    MessageToast.show(i18n.getText("messageDontFieldWerks"));
                    oSelectWerks.setValueState(ValueState.Error);
                    document.getElementsByClassName('textWerks')[0].style.backgroundColor = '#ff0000';
                }
                if (sKeylgort)
                {
                    oQuery.Lgort = sKeylgort;
                    oSelectLgort.setValueState(ValueState.Success);
                }
                else
                {
                    flag = 1;
                    MessageToast.show(i18n.getText("messageDontFieldLgort"));
                    oSelectLgort.setValueState(ValueState.Error);
                    document.getElementsByClassName('textLgort')[0].style.backgroundColor = '#ff0000';
                }
                if (sKeyStor)
                {
                    oQuery.Stor = sKeyStor;
                    oSelectStor.setValueState(ValueState.Success);
                }
                else
                {
                    flag = 1;
                    MessageToast.show(i18n.getText("messageDontFieldStor"));
                    oSelectStor.setValueState(ValueState.Error);
                    document.getElementsByClassName('textStor')[0].style.backgroundColor = '#ff0000';
                }
                if (sKeyRack)
                {
                    oQuery.Rack = sKeyRack;
                    oSelectRack.setValueState(ValueState.Success);
                }
                else
                {
                    flag = 1;
                    MessageToast.show(i18n.getText("messageDontFieldRack"));
                    oSelectRack.setValueState(ValueState.Error);
                    document.getElementsByClassName('textRack')[0].style.backgroundColor = '#ff0000';
                }
                if (sKeyCell)
                {
                    oQuery.Cell = sKeyCell;
                    oSelectCell.setValueState(ValueState.Success);
                }
                else
                {
                    flag = 1;
                    MessageToast.show(i18n.getText("messageDontFieldCell"));
                    oSelectCell.setValueState(ValueState.Error);
                    document.getElementsByClassName('textCell')[0].style.backgroundColor = '#ff0000';
                }
                if (sKeyFloor)
                {
                    oQuery.Floor = sKeyFloor;
                    oSelectFloor.setValueState(ValueState.Success);
                }
                else
                {
                    flag = 1;
                    MessageToast.show(i18n.getText("messageDontFieldFloor"));
                    oSelectFloor.setValueState(ValueState.Error);
                    document.getElementsByClassName('textFloor')[0].style.backgroundColor = '#ff0000';
                }
                if (sKeyType)
                {
                    oQuery.Type = sKeyType;
                    oSelectType.setValueState(ValueState.Success);
                }
                else
                {
                    flag = 1;
                    MessageToast.show(i18n.getText("messageDontFieldType"));
                    oSelectType.setValueState(ValueState.Error);
                    document.getElementsByClassName('textType')[0].style.backgroundColor = '#ff0000';
                }
                if (vQuant)
                {
                    oQuery.Quant= vQuant;
                }
                if (vAufnr)
                {
                    oQuery.Aufnr = vAufnr;
                }
                if (vOldCode)
                {
                    oQuery.OldCodeMpl = vOldCode;
                }
            },

            onButtonCancel: function()
            {
                window.history.go(-2);
            },

            onButtonSave: function()
            {
                var oQuery = { };
                var flag = 0;

                this.getKey(oQuery, flag);
                if (flag == 0) {
                    var oModel = this.getView().getModel();
                    oModel.create("/Screen_TwoSet", oQuery, {
                        success: function(data){
                            window.history.go(-2);
                            MessageBox.success(i18n.getText("messageAllGood"));
                            MessageToast.show(i18n.getText("messageAllGood"));

                        },
                        error: function(error){
                            var manager = Core.getMessageManager();
                            var messages = manager.getMessageModel().getData();
                            manager .removeAllMessages()

                            MessageBox.error(messages[0].message);
                        }
                    })
                }
            },
        });
    });