sap.ui.define([
    "sap/ui/core/mvc/Controller",
    'sap/m/MessageToast',
    'sap/ui/model/json/JSONModel',
    'sap/m/Token',
    "zlocationmanagementv0/1/utils/tt",
    '/sap/ui/core/Core',
    'sap/ui/core/routing/History',
    "sap/m/MessageBox"
    ],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageToast, JSONModel, Token, tt, Core, History, MessageBox) {
        "use strict";
        var oQuery;
        var tableData = [];

        var i18n;

        var vAufnr;

        return Controller.extend("zlocationmanagement.controller.ScreenPartial", {
            onInit: function () {
                var routerX = sap.ui.core.UIComponent.getRouterFor(this);
                routerX.getRoute("ScreenPartial").attachMatched(this.onRouteMatched, this);
            },

            onRouteMatched: function(oCaller)
            {
                var oArgs = oCaller.getParameter("arguments");

                if(!oArgs) {
                    return;
                }

                oQuery = oArgs["?query"];

                if(!oQuery){
                    return;
                }
                var oView = this.getView();

                var windowModel = {
                    Werks: "",
                    lgort: "",
                    Floor: "",
                    Stor: "",
                    Rack: "",
                    Cell: "",
                    Count: ""
                };

                if(oQuery.Aufnr)
                {
                    vAufnr = oQuery.Aufnr;

                    // Изменение Титульника
                    i18n = this.getView().getModel("i18n").getResourceBundle();
                    var sTitle = i18n.getText("titPartial") + ' ' + oQuery.Aufnr;
                    this.getView().byId('page').setTitle(sTitle);
                }

                windowModel.Werks = oQuery.Werks ? oQuery.Werks : "Пусто";
                windowModel.Lgort = oQuery.Lgort ? oQuery.Lgort : "Пусто";
                windowModel.Floor = oQuery.Floor ? oQuery.Floor : "Пусто";
                windowModel.Stor = oQuery.Stor ? oQuery.Stor : "Пусто";
                windowModel.Rack = oQuery.Rack ? oQuery.Rack : "Пусто";
                windowModel.Cell = oQuery.Cell ? oQuery.Cell : "Пусто";
                windowModel.Type = oQuery.Type ? oQuery.Type : "Пусто";
                windowModel.Quant = oQuery.Quant ? oQuery.Quant : "Пусто";

                oView.setModel(new JSONModel(windowModel), "mainData");

                tableData = [
                    {
                        Id: 0,
                        Cnt: "",
                        MplCode: ""
                    },
                    {
                        Id: 1,
                        Cnt: "",
                        MplCode: ""
                    }
                ];

                var myModel = oView.getModel("mainData");

                // oView.getModel().read("/List_NamingSet", {
                //     filters: [],
                //     success: function(oEvt) {
                //     //обрабатываем положительный ответ
                //         alert('kek!')
                //     },
                //     error: function(oEvt) {
                //     //обрабатываем ошибку
                //         alert('kek :(')
                //     }
                // });

                oView.setModel(new JSONModel(tableData), "tabs");
            },

            execute: function(index) {

                let self = this;

                var openPromise = tt.open(this)

                openPromise.then(
                    //success
                    (result) => {
                        var modelX = self.oView.getModel("tabs");
                        var prop = modelX.getProperty('/')
                        prop[index].MplCode = result.werks + "_" + result.lgort + "_" +
                                                result.floor + "_" + result.stor.substring(2, 4) + "_" +
                                                result.rack.substring(2, 4) + "_" + result.cell.substring(2, 4) + "_" +
                                                result.type;
                        modelX.setProperty('/', prop)

                    },
                    //error
                    (oError) => {

                    }
                )
            },
            onClickAdd: function(index)
            {
                var oTable = this.getView().byId("favoritList");
			    // oTable.addItem(oItem);
                var modelX = this.oView.getModel("tabs");
                tableData.push({Id: tableData.length, Cnt: "", MplCode: "" });
                modelX.setProperty('/', tableData)

            },

            onClickClear: function(index)
            {
                var oTable = this.getView().byId("favoritList");
			    // oTable.addItem(oItem);
                var modelX = this.oView.getModel("tabs");
                tableData.splice(tableData.length - 1,  tableData.length);
                modelX.setProperty('/', tableData)

            },


            onChage: function (value)
            {
                // var text = value.mParameters.value;
                var text = value.text;
                var oMultiInput = oView.byId("fieldMPL");

				return new Token({key: text, text: text});
            },

            onClickSave: function(oEvent)
            {
                var modelX = this.oView.getModel("tabs");
                var prop = modelX.getProperty('/');
                var oModel = this.getView().getModel();
                var vQuery = { };
                var flagError = 0;
                for (let index = 0; index < prop.length; index++)
                {
                    if (!prop[index].Cnt)
                    {
                        flagError = 1;
                        MessageBox.error(i18n.getText("messageDontFieldCount"));
                        break;
                    }
                    if (!prop[index].MplCode)
                    {
                        flagError = 1;
                        MessageBox.error(i18n.getText("messageDontFieldCodeMPL"));
                        break;
                    }
                }
                if (flagError == 1)
                {
                    return;
                }

                var aFilter = [];
                var valueFieldQuant = oQuery.Quant;

                for (let index = 0; index < prop.length; index++)
                {
                    // aFilter.splice(0, aFilter.length);
                    if(oQuery.Code == prop[index].MplCode)
                    {
                        flagError = 1;
                        MessageBox.warning(i18n.getText("messageDoubleCode"));
                        break;
                    }

                    if (vAufnr)
                    {
                        vQuery.Aufnr = vAufnr;
                    }
                    if (oQuery.Code)
                    {
                        vQuery.OldCodeMpl = oQuery.Code;
                    }
                    if (prop[index].Cnt)
                    {
                        vQuery.Quant = prop[index].Cnt;
                        valueFieldQuant -= prop[index].Cnt;
                        valueFieldQuant = valueFieldQuant.toFixed(3);

                        if ( valueFieldQuant < 0 || prop[index].Cnt == 0)
                        {
                            flagError = 1;
                            MessageBox.warning(i18n.getText("messageAbautMachCount"));
                            break;
                        }
                    }
                    if (prop[index].MplCode)
                    {
                        vQuery.CodeMpl = prop[index].MplCode;
                    }
                    vQuery.Partial = true;

                    var oFilterAufnr = new sap.ui.model.Filter(
                        {
                            path: "Aufnr",
                            operator: sap.ui.model.FilterOperator.EQ,
                            value1: vQuery.Aufnr
                        }
                    )
                    var oFilterOldCodeMpl = new sap.ui.model.Filter(
                        {
                            path: "OldCode",
                            operator: sap.ui.model.FilterOperator.EQ,
                            value1: vQuery.OldCodeMpl
                        }
                    )
                    var oFilterQuant = new sap.ui.model.Filter(
                        {
                            path: "Quant",
                            operator: sap.ui.model.FilterOperator.EQ,
                            value1: vQuery.Quant
                        }
                    )
                    var oFilterCodeMpl = new sap.ui.model.Filter(
                        {
                            path: "CodeMpl",
                            operator: sap.ui.model.FilterOperator.EQ,
                            value1: vQuery.CodeMpl
                        }
                    )
                    var oFilterPartial = new sap.ui.model.Filter(
                        {
                            path: "Partial",
                            operator: sap.ui.model.FilterOperator.EQ,
                            value1: vQuery.Partial
                        }
                    )

                    aFilter.push(oFilterAufnr, oFilterOldCodeMpl, oFilterQuant, oFilterCodeMpl, oFilterPartial)
                }

                if (flagError == 1)
                {
                    return;
                }

                oModel.read("/Screen_PartialSet", {
                    filters: aFilter,
                    success: function(data){
                        window.history.go(-1);
                        MessageBox.success(i18n.getText("messageAllGood"));
                        MessageToast.show(i18n.getText("messageAllGood"));

                    },
                    error: function(error){
                        var manager = Core.getMessageManager();
                        var messages = manager.getMessageModel().getData();
                        manager .removeAllMessages()

                        window.history.go(-1);

                        for (let index = 0; index < messages.length; index++)
                        {
                            MessageBox.error(messages[index].message);
                        }

                    }
                });
            },
        });
    });
