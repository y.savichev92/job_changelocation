sap.ui.define([
    "sap/ui/core/mvc/Controller",
    'sap/m/MessageToast',
    'sap/ui/core/routing/History'
    ],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageToast, History) {
        "use strict";
        var flag;
        return Controller.extend("zlocationmanagement.controller.View1", {            
            onInit: function () {
                var routerX = sap.ui.core.UIComponent.getRouterFor(this);
                routerX.getRoute("RouteMainView").attachMatched(this.onRouteMatched, this);
                flag = 0;
            },
            onRouteMatched: function()
            {
                var oQuery = { };

                // this.getOwnerComponent().getTargets().display("SelectionScreen");
	            
                if(flag == 0)
                {
                    flag = 1;

                    var oModel = this.getView().getModel();
                    var routerX = sap.ui.core.UIComponent.getRouterFor(this);

                    oModel.read( "/Get_HomeWork_UserSet",{filters: [
                    new sap.ui.model.Filter( 'Werks', sap.ui.model.FilterOperator.EQ, '#'
                    )],
                    success: function(data)
                    {   
                        var oNameWerks = data.results[0].NameWerks;
                        var oWerks = data.results[0].Werks;
                        var oNameLgort = data.results[0].NameLgort;
                        var oLgort = data.results[0].Lgort;
                        
                        oQuery.NameWerks = oNameWerks;
                        oQuery.Werks = oWerks;
                        oQuery.NameLgort = oNameLgort;
                        oQuery.Lgort = oLgort;
                    
                        routerX.navTo("SelectionScreen", {
                            "?query" : oQuery
                        });
                
                    },
                    error:function(error)
                    {
                        routerX.navTo("SelectionScreen", {
                            "?query" : oQuery
                        });
                    }
                    });
                }
                else
                {
                    history.go(-1);
                }
                
            }, 
        });
    });
