sap.ui.define([
    "sap/ui/core/mvc/Controller",
    'sap/m/MessageToast',
    'sap/ui/core/routing/History',
    '/sap/ui/core/Core',
    "sap/m/MessageBox"
    ],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageToast, History, Core, MessageBox) {
        "use strict";
        var oQuery, mainFilter;

        var i18n;

        return Controller.extend("zlocationmanagement.controller.Scanner", {
            onInit: function () {
                var routerY = sap.ui.core.UIComponent.getRouterFor(this);
                routerY.getRoute("Scanner").attachMatched(this.onRouteMatched, this);
            },

            onRouteMatched: function(oCaller)
            {
                var sKeyWerks = this.getView().byId('inputQR');
                var aFilter = [];

                sKeyWerks.setValue("");

                var oArgs = oCaller.getParameter("arguments");
                if(!oArgs){
                    return;
                }

                oQuery = oArgs["?query"];

                if(!oQuery){
                    return;
                }
                if(oQuery.Aufnr)
                {
                    // Изменение Титульника
                    i18n = this.getView().getModel("i18n").getResourceBundle();
                    var sTitle = i18n.getText("titChangeMPL") + ' ' + oQuery.Aufnr;
                    this.getView().byId('page').setTitle(sTitle);
                }

                mainFilter = aFilter;
            },

            onButtonContinue: function()
            {
                // var routerX = sap.ui.core.UIComponent.getRouterFor(this);
                // routerX.navTo("ScreenNext", {
                //     "?query" : oQuery
                // });

            },

            onButtonManual: function ()
            {
                var routerX = sap.ui.core.UIComponent.getRouterFor(this);
                routerX.navTo("ScreenChange", {
                    "?query" : oQuery
                });
            },

            onGetQR: function(oEvent)
            {
                var oQueryTo = { };
                if (oQuery.Aufnr)
                {
                    oQueryTo.Aufnr = oQuery.Aufnr;
                }
                if (oQuery.Quant)
                {
                    oQueryTo.Quant = oQuery.Quant;
                }
                if (oQuery.Code)
                {
                    oQueryTo.OldCodeMpl = oQuery.Code;
                }
                oQueryTo.CodeMpl = oEvent.mParameters.value;

                var oModel = this.getView().getModel();
                oModel.create("/Screen_TwoSet", oQueryTo, {
                    success: function(data){
                        window.history.go(-1);
                        MessageBox.success(i18n.getText("messageAllGood"));
                        MessageToast.show(i18n.getText("messageAllGood"));

                    },
                    error: function(error){
                        var manager = Core.getMessageManager();
                        var messages = manager.getMessageModel().getData();
                        manager .removeAllMessages()

                        MessageBox.error(messages[0].message);
                    }
                })
            },

        });
    });
