sap.ui.define([
    'sap/ui/core/library',
    "sap/ui/core/mvc/Controller",
    'sap/m/MessageToast',
    '/sap/ui/core/Core',
    "sap/m/MessageBox"
    ],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (coreLibrary, Controller, MessageToast, Core, MessageBox) {
        "use strict";

        var ValueState = coreLibrary.ValueState;

        var oQuery;
        var oAufnr, oKeyCode;
        var oValidatedComboBox;

        var i18n;

        return Controller.extend("zlocationmanagement.controller.ScreenNotMPL", {
            onInit: function () {
                var routerY = sap.ui.core.UIComponent.getRouterFor(this);
                routerY.getRoute("ScreenNotMPL").attachMatched(this.onRouteMatched, this);
            },

            onRouteMatched: function(oCaller)
            {
                var sKeyQR = this.getView().byId('ScannerQR');
                sKeyQR.setValue("");

                i18n = this.getView().getModel("i18n").getResourceBundle();

                var aFilter = [];

                var oArgs = oCaller.getParameter("arguments");
                if(!oArgs){
                    return;
                }

                oQuery = oArgs["?query"];

                if(!oQuery){
                    return;
                }

                if(oQuery.Aufnr)
                {
                    var oFilterAufnr = new sap.ui.model.Filter(
                    {
                        path: 'Aufnr',
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: oQuery.Aufnr
                    });

                    aFilter.push(oFilterAufnr);
                }

                if(oQuery.Werks)
                {
                    var oFilterWerks = new sap.ui.model.Filter(
                    {
                        path: 'Werks',
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: oQuery.Werks
                    });

                    aFilter.push(oFilterWerks);
                }

                if(oQuery.Lgort)
                {
                    var oFilterLgort = new sap.ui.model.Filter(
                    {
                        path: 'Lgort',
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: oQuery.Lgort
                    });

                    aFilter.push(oFilterLgort);
                }

                var oTable = this.byId("favoritList").getBinding('items');
                oTable.filter(aFilter);
            },

            selectSetMPL: function()
            {
                var routerX = sap.ui.core.UIComponent.getRouterFor(this);
                var oQuery = { };

                if (oAufnr)
                {
                    oQuery.Aufnr = oAufnr;
                    routerX.navTo("ScreenChange", {
                        "?query" : oQuery
                    });
                }
                else
                {
                    MessageBox.warning(i18n.getText("messageWarningSelectOneAufnr"));
                }
            },

            chengeQR: function(oEvent)
            {
                oKeyCode = oEvent.mParameters.value;
                oValidatedComboBox = oEvent.getSource();
            },

            onGetQR: function (sAufnr, sQuant, sIndex)
            {
                if (oKeyCode) {
                    var oQuery = { };
                    if (sAufnr)
                    {
                        oQuery.Aufnr = sAufnr;
                    }
                    if (sQuant)
                    {
                        oQuery.Quant = sQuant;
                    }
                    oQuery.CodeMpl = oKeyCode;

                    var oModel = this.getView().getModel();
                    oModel.create("/Screen_TwoSet", oQuery, {
                        success: function(data){
                            oValidatedComboBox.setValueState(ValueState.Success);

                            MessageBox.success(i18n.getText("messageAllGood"));
                            MessageToast.show(i18n.getText("messageAllGood"));
                        },
                        error: function(error){
                            var manager = Core.getMessageManager();
                            var messages = manager.getMessageModel().getData();
                            manager .removeAllMessages()

                            // document.getElementsByClassName('sapMListTblRow')[sIndex].style.backgroundColor = '#ff0000';

                            oValidatedComboBox.setValueState(ValueState.Error);
                            MessageBox.error(messages[0].message);
                        }
                    })
                }
                else{
                    oValidatedComboBox.setValueState(ValueState.None);
                }

            },

            onSelect: function (oEvent)
            {
                var sKeyAufnr = this.getView().byId('favoritList')._aSelectedPaths[0];
                let intex = 0;

                var first = sKeyAufnr.indexOf("'", intex);
                if (first != -1)
                {
                    intex = first + 1;
                }
                var finish = sKeyAufnr.indexOf("'", intex);

                oAufnr = sKeyAufnr.substring(first + 1, finish);
            },

        });
    });