sap.ui.define([
    'sap/ui/core/library',
    "sap/ui/core/mvc/Controller",
    'sap/m/MessageToast',
    "sap/m/MessageBox"
    ],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */

    function (coreLibrary, Controller, MessageToast, MessageBox) {
        "use strict";

        var i18n;

        var ValueState = coreLibrary.ValueState;

        var oSelectWerks, oSelectLgort, oSelectStor, oSelectRack, oSelectCell, oSelectFloor, oSelectNaming, oSelectCode;
        var oBindingLgort, oBindingStor, oBindingRack, oBindingCell, oBindingFloor, oBindingNaming, oBindingCode;
        var sKeyWerks, sKeylgort, sKeyStor, sKeyRack, sKeyCell,
            sKeyFloor, sKeyNaming, sTypeLgort, sKeyAufnr, sKeyCodeMPL;

        var oFilterWerks, oFilterLgort, oFilterStor, oFilterRack, oFilterCell, oFilterFloor, oFilterNaming;

        var flag = 1;
        var firctInput = 1;

        return Controller.extend("zlocationmanagement.controller.SelectionScreen", {
            onInit: function () {
                firctInput = 0;
                var routerX = sap.ui.core.UIComponent.getRouterFor(this);
                routerX.getRoute("SelectionScreen").attachMatched(this.onRouteMatched, this);
            },

            onRouteMatched: function(oCaller)
            {
                i18n = this.getView().getModel("i18n").getResourceBundle();
                this.castToObjectSrv();

                // Считывание переданых данных с URL
                var oArgs = oCaller.getParameter("arguments");

                if(!oArgs){
                    return;
                }

                var oQuery = oArgs["?query"];

                if(!oQuery){
                    return;
                }

                var oSelectWerksText = this.getView().byId('werksCore');
                var oSelectLgortText = this.getView().byId('lgortCore');

                if (oQuery.NameWerks && oQuery.Werks && firctInput == 0)
                {
                    firctInput = 1;
                    oSelectWerksText.setText(oQuery.NameWerks);
                    oSelectWerks.setSelectedKey(oQuery.Werks);

                    if (oQuery.NameLgort && oQuery.Lgort)
                    {
                        oSelectLgortText.setText(oQuery.NameLgort);
                        oSelectLgort.setSelectedKey(oQuery.Lgort);
                    };
                };
                flag = 0;
                this.onChangeWerks();
                this.getValueFeilde();

                if (!sKeyWerks) {
                    oSelectWerks.setValueState(ValueState.Error);
                } else {
                    oSelectWerks.setValueState(ValueState.Success);
                }
                if (!sKeylgort) {
                    oSelectLgort.setValueState(ValueState.Error);
                } else {
                    oSelectLgort.setValueState(ValueState.Success);
                }
            },

            checkFields: function()
            {
                this.getValueFeilde();

                if (!sKeyWerks)
                {
                    MessageBox.error(i18n.getText("messageDontFieldWerks"));
                    return 1;
                }
                if (!sKeylgort)
                {
                    MessageBox.error(i18n.getText("messageDontFieldLgort"));
                    return 2;
                }
                return 0;
            },

            castToObjectSrv: function()
            {
                // К какому объекту oData будет обращение
                oSelectWerks = this.getView().byId('werksComboBox');
                oSelectLgort = this.getView().byId('lgortComboBox');
                oSelectStor = this.getView().byId('storComboBox');
                oSelectRack = this.getView().byId('rackComboBox');
                oSelectCell = this.getView().byId('cellComboBox');
                oSelectFloor = this.getView().byId('floorComboBox');
                oSelectNaming = this.getView().byId('namingComboBox');
                oSelectCode = this.getView().byId('codeComboBox');
            },

            getValueFeilde: function()
            {
                // Получение значения указание в поле на экране
                sKeyWerks = this.getView().byId('werksComboBox').getSelectedKey();
                sKeylgort = this.getView().byId('lgortComboBox').getSelectedKey();
                sKeyStor = this.getView().byId('storComboBox').getSelectedKey();
                sKeyRack = this.getView().byId('rackComboBox').getSelectedKey();
                sKeyCell = this.getView().byId('cellComboBox').getSelectedKey();
                sKeyFloor = this.getView().byId('floorComboBox').getSelectedKey();
                sKeyNaming = this.getView().byId('namingComboBox').getSelectedKey();
                sKeyAufnr = this.getView().byId("aufnrInput").getValue();
                sTypeLgort =  this.getView().byId("typeInput").getValue();
                sKeyCodeMPL = this.getView().byId("codeComboBox").getValue();
            },

            onBildData: function()
            {
                this.castToObjectSrv();

                oBindingLgort = oSelectLgort.getBinding("items");
                oBindingStor = oSelectStor.getBinding("items");
                oBindingRack = oSelectRack.getBinding("items");
                oBindingCell = oSelectCell.getBinding("items");
                oBindingFloor = oSelectFloor.getBinding("items");
                oBindingNaming = oSelectNaming.getBinding("items");
                oBindingCode = oSelectCode.getBinding("items");

                this.getValueFeilde();

                oFilterWerks = new sap.ui.model.Filter(
                {
                    path: 'Werks',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeyWerks
                });
                oFilterLgort = new sap.ui.model.Filter(
                {
                    path: 'Lgort',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeylgort
                });
                oFilterStor = new sap.ui.model.Filter(
                {
                    path: 'Stor',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeyStor
                });
                oFilterRack = new sap.ui.model.Filter(
                {
                    path: 'Rack',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeyRack
                });
                oFilterCell = new sap.ui.model.Filter(
                {
                    path: 'Cell',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeyCell
                });
                oFilterFloor = new sap.ui.model.Filter(
                {
                    path: 'Floor',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeyFloor
                });
                oFilterNaming = new sap.ui.model.Filter(
                {
                    path: 'Name',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeyNaming
                });
            },

            onChangeWerks: function(oEvent)
            {
                if (flag == 1)
                {
                    oSelectLgort.setSelectedKey("");
                }
                flag = 1;
                oSelectStor.setSelectedKey("");
                oSelectRack.setSelectedKey("");
                oSelectCell.setSelectedKey("");
                oSelectFloor.setSelectedKey("");
                oSelectNaming.setSelectedKey("");

                this.onBildData();

                // var sSelectedKey = oSelectWerks.getSelectedKey(),
				    // sValue = oSelectWerks.getValue();

                if (!sKeyWerks) {
                    oSelectWerks.setValueState(ValueState.Error);
                } else {
                    oSelectWerks.setValueState(ValueState.Success);
                }
                // var sSelectedKey = oSelectLgort.getSelectedKey(),
                // sValue = oSelectLgort.getValue();

                if (!sKeylgort) {
                    oSelectLgort.setValueState(ValueState.Error);
                } else {
                    oSelectLgort.setValueState(ValueState.Success);
                }

                var aFilter = [];
                var aFilterLgort = [];

                aFilterLgort.push(oFilterWerks);
                aFilter.push(oFilterWerks, oFilterLgort);

                oBindingCode.filter(aFilter);
                oBindingLgort.filter(aFilterLgort);
                oBindingStor.filter(aFilter);
                oBindingRack.filter(aFilter);
                oBindingCell.filter(aFilter);
                oBindingFloor.filter(aFilter);
                oBindingNaming.filter(aFilter);

            },

            onChangeLgort: function(oEvent)
            {
                oSelectStor.setSelectedKey("");
                oSelectRack.setSelectedKey("");
                oSelectCell.setSelectedKey("");
                oSelectFloor.setSelectedKey("");
                oSelectNaming.setSelectedKey("");

                this.onBildData();

                if (!sKeylgort) {
                    oSelectLgort.setValueState(ValueState.Error);
                } else {
                    oSelectLgort.setValueState(ValueState.Success);
                }

                var oSelect, oBinding, aFilterLgort, aFilterWerks;
                var sKeyWerks = this.getView().byId('werksComboBox').getSelectedKey();
                oSelect = this.getView().byId('lgortComboBox');
                oBinding = this.byId('werksComboBox').getBinding('items');
                aFilterLgort = [];
                aFilterWerks = new sap.ui.model.Filter({
                    path: 'Werks',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sKeyWerks
                });
                aFilterLgort.push(aFilterWerks);
                oBinding.filter(aFilterLgort);

                this.onBildData();

                var aFilter = [];

                aFilter.push(oFilterWerks, oFilterLgort);

                oBindingCode.filter(aFilter);
                oBindingStor.filter(aFilter);
                oBindingRack.filter(aFilter);
                oBindingCell.filter(aFilter);
                oBindingFloor.filter(aFilter);
                oBindingNaming.filter(aFilter);
            },

            onChangeStor: function()
            {
                oSelectRack.setSelectedKey("");
                oSelectCell.setSelectedKey("");
                oSelectNaming.setSelectedKey("");
                var aFilter = [];

                this.onBildData();
                aFilter.push(oFilterWerks, oFilterLgort, oFilterStor, oFilterFloor);

                oBindingRack.filter(aFilter);
                oBindingCell.filter(aFilter);
                oBindingNaming.filter(aFilter);
            },

            onChangeRack: function()
            {
                oSelectCell.setSelectedKey("");
                oSelectNaming.setSelectedKey("");
                var aFilter = [];

                this.onBildData();
                aFilter.push(oFilterWerks, oFilterLgort, oFilterStor, oFilterRack, oFilterFloor);

                oBindingCell.filter(aFilter);
                oBindingNaming.filter(aFilter);
            },

            onChangeCell: function()
            {
                oSelectNaming.setSelectedKey("");
                var aFilter = [];

                this.onBildData();
                aFilter.push(oFilterWerks, oFilterLgort, oFilterStor, oFilterRack,  oFilterCell, oFilterFloor);

                oBindingNaming.filter(aFilter);
            },

            onChangeFloor: function()
            {
                oSelectStor.setSelectedKey("");
                oSelectRack.setSelectedKey("");
                oSelectCell.setSelectedKey("");
                oSelectNaming.setSelectedKey("");

                var aFilter = [];

                this.onBildData();
                aFilter.push(oFilterWerks, oFilterLgort, oFilterFloor);

                oBindingStor.filter(aFilter);
                oBindingRack.filter(aFilter);
                oBindingCell.filter(aFilter);
                oBindingNaming.filter(aFilter);
            },

            onChangeNaming: function()
            {
                // var aFilter = [];

                // this.onBildData();
                // aFilter.push(oFilterWerks, oFilterLgort, oFilterStor, oFilterRack,  oFilterCell, oFilterFloor, oFilterNaming);

                // oBindingStor.filter(aFilter);
                // oBindingRack.filter(aFilter);
                // oBindingCell.filter(aFilter);
                // oBindingFloor.filter(aFilter);
                // // oBindingNaming.filter(aFilter);
            },

            onChangeAufnr: function()
            {

            },
            onChangeCode_MPL: function()
            {

            },

            getKey(oQuery)
            {
                if (sKeyWerks)
                {
                    oQuery.Werks = sKeyWerks;
                }
                if (sKeylgort)
                {
                    oQuery.Lgort = sKeylgort;
                }
                if (sKeyStor)
                {
                    oQuery.Stor = sKeyStor;
                }
                if (sKeyRack)
                {
                    oQuery.Rack = sKeyRack;
                }
                if (sKeyCell)
                {
                    oQuery.Cell = sKeyCell;
                }
                if (sKeyFloor)
                {
                    oQuery.Floor = sKeyFloor;
                }
                if (sKeyNaming)
                {
                    oQuery.Naming = sKeyNaming;
                }
                if (sTypeLgort)
                {
                    oQuery.Type = sTypeLgort;
                }

                if (sKeyAufnr)
                {
                    oQuery.Aufnr = sKeyAufnr;
                }
                if (sKeyCodeMPL)
                {
                    oQuery.Code = sKeyCodeMPL;
                }
            },

            onClickNext: function()
            {
                if (this.checkFields())
                {
                    return 101;
                }

                var routerX = sap.ui.core.UIComponent.getRouterFor(this);
                var oQuery = { };

                this.getKey(oQuery);

                routerX.navTo("ScreenNext", {
                    "?query" : oQuery
                });
            },

            onClickNotMPL: function()
            {
                // getRoute();
                if (this.checkFields())
                {
                    return 101;
                }

                var oQuery = { };

                this.getKey(oQuery);

                var routerX = sap.ui.core.UIComponent.getRouterFor(this);
                routerX.navTo("ScreenNotMPL", {
                    "?query" : oQuery
                });
            },

            // cancel: function()
            // {
            //     var oHistory = sap.ui.core.routing.History.getInstance(),
            //         sPreviousHash = oHistory.getPreviousHash();
            // },
        });
    });
