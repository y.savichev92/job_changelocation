sap.ui.define(
    [
        "sap/ui/core/mvc/Controller",
        'sap/m/MessageToast'
    ],
    function(BaseController, MessageToast)
    {
        "use strict";

        return BaseController.extend("zlocationmanagementv0.1.controller.ScreenNext", {
            onInit()
            {
                //Привязываемся к событию навигации на это окно
                var routerX = sap.ui.core.UIComponent.getRouterFor(this);
                routerX.getRoute("ScreenNext").attachMatched(this.onRouteMatched, this);
            },

            selectChange: function(sType, sWerks, sLgort, sCode,  sFloor, sStor, sRack, sCell, sAufnr, sQuant)
            {
                // Привязка к роутеру с провисаными экраноми
                var routerX = sap.ui.core.UIComponent.getRouterFor(this);
                var oQuery = { };

                oQuery.Werks = sWerks ? sWerks : "Пусто";
                oQuery.Lgort = sLgort ? sLgort : "Пусто";
                oQuery.Stor = sStor ? sStor : "Пусто";
                oQuery.Rack = sRack ? sRack : "Пусто";
                oQuery.Code = sCode ? sCode : "Пусто";
                oQuery.Floor = sFloor ? sFloor : "Пусто";
                oQuery.Type = sType ? sType : "Пусто";
                oQuery.Cell = sCell ? sCell : "Пусто";
                oQuery.Quant = sQuant ? sQuant : "Пусто";
                if (sAufnr)
                {
                    oQuery.Aufnr = sAufnr;
                }

                // MessageToast.show("werks = " + sWerks + ", lgort = " + sLgort);
                // Вызов нужного экрана по URL с передачей значений
                routerX.navTo("Scanner", {
                    "?query" : oQuery
                });
            },

            selectPartial: function(sType, sNaming, sWerks, sLgort, sCode,  sFloor, sStor, sRack, sCell, sAufnr, sQuant)
            {
                var routerX = sap.ui.core.UIComponent.getRouterFor(this);
                var oQuery = { };

                oQuery.Werks = sWerks ? sWerks : "Пусто";
                oQuery.Lgort = sLgort ? sLgort : "Пусто";
                oQuery.Stor = sStor ? sStor : "Пусто";
                oQuery.Rack = sRack ? sRack : "Пусто";
                oQuery.Code = sCode ? sCode : "Пусто";
                oQuery.Floor = sFloor ? sFloor : "Пусто";
                oQuery.Type = sType ? sType : "Пусто";
                // oQuery.Naming = sNaming ? sNaming : "Пусто";
                oQuery.Cell = sCell ? sCell : "Пусто";
                oQuery.Quant = sQuant ? sQuant : "Пусто";
                if (sAufnr)
                {
                    oQuery.Aufnr = sAufnr;
                }

                routerX.navTo("ScreenPartial", {
                  "?query" : oQuery
                });
            },

            onRouteMatched: function(oCaller) {
                // Считывание переданых данных с URL
                var oArgs = oCaller.getParameter("arguments");

                if(!oArgs){
                    return;
                }

                var oQuery = oArgs["?query"];

                if(!oQuery){
                    return;
                }

                var aFilter = [];

                if(oQuery.Werks)
                {
                    var oFilterWerks = new sap.ui.model.Filter(
                    {
                        path: 'Werks',
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: oQuery.Werks
                    });

                    aFilter.push(oFilterWerks);
                }
                if(oQuery.Lgort)
                {
                    var oFilterLgort = new sap.ui.model.Filter(
                    {
                        path: 'Lgort',
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: oQuery.Lgort
                    });

                    aFilter.push(oFilterLgort);
                }
                if(oQuery.Stor)
                {
                    var oFilterStor = new sap.ui.model.Filter(
                    {
                        path: 'Stor',
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: oQuery.Stor
                    });

                    aFilter.push(oFilterStor);
                }
                if(oQuery.Cell)
                {
                    var oFilterCell = new sap.ui.model.Filter(
                    {
                        path: 'Cell',
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: oQuery.Cell
                    });

                    aFilter.push(oFilterCell);
                }
                if(oQuery.Floor)
                {
                    var oFilterFloor = new sap.ui.model.Filter(
                    {
                        path: 'Floor',
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: oQuery.Floor
                    });

                    aFilter.push(oFilterFloor);
                }
                if(oQuery.Naming)
                {
                    var oFilterNaming = new sap.ui.model.Filter(
                    {
                        path: 'Naming',
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: oQuery.Naming
                    });

                    aFilter.push(oFilterNaming);
                }
                if(oQuery.Aufnr)
                {
                    var oFilterAufnr = new sap.ui.model.Filter(
                    {
                        path: 'Aufnr',
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: oQuery.Aufnr
                    });

                    aFilter.push(oFilterAufnr);
                }
                if(oQuery.Type)
                {
                    var oFilterType = new sap.ui.model.Filter(
                    {
                        path: 'Type',
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: oQuery.Type
                    });

                    aFilter.push(oFilterType);
                }
                if(oQuery.Code)
                {
                    var oFilterCode = new sap.ui.model.Filter(
                    {
                        path: 'CodeMpl',
                        operator: sap.ui.model.FilterOperator.EQ,
                        value1: oQuery.Code
                    });

                    aFilter.push(oFilterCode);
                }

            var oTable = this.byId("favoritList").getBinding('items');
            oTable.filter(aFilter);
            }
        });
    });