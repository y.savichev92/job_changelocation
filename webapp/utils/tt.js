sap.ui.define(
    [
        'sap/ui/core/library',
        "sap/ui/core/Fragment",
        "sap/ui/model/json/JSONModel",
        "sap/m/MessageBox"
    ],
    function (
        coreLibrary,
        Fragment,
        JSONModel,
        MessageBox
    )
    {
        "use strict";
        var ValueState = coreLibrary.ValueState;

        var tableListBox = { };
        var vDialog;
        var  oFilterWerks , oFilterLgort , oFilterFloor , oFilterStor , oFilterRack;

        var oComboBoxLgort, oComboBoxFloor, oComboBoxStor, oComboBoxRack, oComboBoxCell;

        return {
            open: function (parent, mplCode) {

                let self = this;

                return new Promise((successCallback, failCallback) => {

                    self._pDialog = Fragment.load({
                        name: "zlocationmanagementv0.1.fragment.tt",
                        controller: self
                    }).then(
                        //success
                        (oDialog) =>  {
                            vDialog = oDialog;
                            oDialog.setModel(parent.getView().getModel());
                            oDialog.setModel(new JSONModel(), "selected");

                            tableListBox.Type = [ { Type: "Пример",
                                                    Num: "01" },
                                                { Type: "S",
                                                    Num: "02" },
                                                    { Type: "Поехали",
                                                    Num: "03"
                                                }];

                            oDialog.setModel(new JSONModel(tableListBox), "Table");
                            parent.getView().addDependent();

                            oDialog.setEscapeHandler(() => {
                                oDialog.close();
                                failCallback()
                            })

                            self.successCallback = successCallback;

                            oDialog.getModel().read("/List_LgortSet", {
                                filters: [],
                                success: function(oEvt) {
                                //обрабатываем положительный ответ
                                    tableListBox.Lgort = oEvt.results;
                                    oDialog.getModel("Table").setProperty('/', tableListBox);
                                },
                                error: function(oEvt) {
                                //обрабатываем ошибку

                                }
                            });
                            oDialog.getModel().read("/List_FloorSet", {
                                filters: [],
                                success: function(oEvt) {
                                //обрабатываем положительный ответ
                                    tableListBox.Floor = oEvt.results;
                                    oDialog.getModel("Table").setProperty('/', tableListBox);
                                },
                                error: function(oEvt) {
                                //обрабатываем ошибку

                                }
                            });
                            oDialog.getModel().read("/List_StorSet", {
                                filters: [],
                                success: function(oEvt) {
                                //обрабатываем положительный ответ
                                    tableListBox.Stor = oEvt.results;
                                    oDialog.getModel("Table").setProperty('/', tableListBox);
                                },
                                error: function(oEvt) {
                                //обрабатываем ошибку

                                }
                            });
                            oDialog.getModel().read("/List_RackSet", {
                                filters: [],
                                success: function(oEvt) {
                                //обрабатываем положительный ответ
                                    tableListBox.Rack = oEvt.results;
                                    oDialog.getModel("Table").setProperty('/', tableListBox);
                                },
                                error: function(oEvt) {
                                //обрабатываем ошибку

                                }
                            });
                            oDialog.getModel().read("/List_CellSet", {
                                filters: [],
                                success: function(oEvt) {
                                //обрабатываем положительный ответ
                                    tableListBox.Cell = oEvt.results;
                                    oDialog.getModel("Table").setProperty('/', tableListBox);
                                },
                                error: function(oEvt) {
                                //обрабатываем ошибку

                                }
                            });

                            self._dialog = oDialog;
                            oDialog.open();
                        },
                        //error
                        (oError) => {
                            alert('error')
                        }
                    )
                });
            },
            // Нажатие КНОПКИ
            acceptClick: function () {
                var oModel = this._dialog.getModel("selected");

                if(!oModel.getProperty("/werks") || !oModel.getProperty("/lgort")
                || !oModel.getProperty("/floor") || !oModel.getProperty("/stor")
                || !oModel.getProperty("/rack") || !oModel.getProperty("/cell")
                || !oModel.getProperty("/type"))
                {
                    MessageBox.warning("Все поля должны быть заполнеными");
                    return;
                }
                this._dialog.close();
                this.successCallback(oModel.oData);
            },

            onChangeWerks: function (oEvent) {
                var oValidatedComboBox = oEvent.getSource(),
                    sSelectedKey = oValidatedComboBox.getSelectedKey();

                if (!sSelectedKey) {
                    oValidatedComboBox.setValueState(ValueState.Error);
                } else {
                    oValidatedComboBox.setValueState(ValueState.Success);
                }
                var oModel = this._dialog.getModel("selected");
                oModel.setProperty("/lgort").clear;
                oModel.setProperty("/floor").clear;
                oModel.setProperty("/stor").clear;
                oModel.setProperty("/rack").clear;
                oModel.setProperty("/cell").clear;
                if(oComboBoxLgort)
                {
                    oComboBoxLgort.setValueState(ValueState.Error);
                }
                if(oComboBoxFloor)
                {
                    oComboBoxFloor.setValueState(ValueState.Error);
                }
                if(oComboBoxStor)
                {
                    oComboBoxStor.setValueState(ValueState.Error);
                }
                if(oComboBoxRack)
                {
                    oComboBoxRack.setValueState(ValueState.Error);
                }
                if(oComboBoxCell)
                {
                    oComboBoxCell.setValueState(ValueState.Error);
                }

                oFilterWerks = new sap.ui.model.Filter(
                {
                    path: 'Werks',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sSelectedKey
                });

                var aFilter = [];
                aFilter.push(oFilterWerks);

                vDialog.getModel().read("/List_LgortSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Lgort = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
                vDialog.getModel().read("/List_FloorSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Floor = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
                vDialog.getModel().read("/List_StorSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Stor = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
                vDialog.getModel().read("/List_RackSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Rack = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
                vDialog.getModel().read("/List_CellSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Cell = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
            },

            onChangeLgort: function (oEvent) {
                oComboBoxLgort = oEvent.getSource();
                var sSelectedKey = oComboBoxLgort.getSelectedKey();

                if (!sSelectedKey) {
                    oComboBoxLgort.setValueState(ValueState.Error);
                } else {
                    oComboBoxLgort.setValueState(ValueState.Success);
                }
                var oModel = this._dialog.getModel("selected");
                oModel.setProperty("/floor").clear;
                oModel.setProperty("/stor").clear;
                oModel.setProperty("/rack").clear;
                oModel.setProperty("/cell").clear;
                if(oComboBoxFloor)
                {
                    oComboBoxFloor.setValueState(ValueState.Error);
                }
                if(oComboBoxStor)
                {
                    oComboBoxStor.setValueState(ValueState.Error);
                }
                if(oComboBoxRack)
                {
                    oComboBoxRack.setValueState(ValueState.Error);
                }
                if(oComboBoxCell)
                {
                    oComboBoxCell.setValueState(ValueState.Error);
                }

                oFilterLgort = new sap.ui.model.Filter(
                {
                    path: 'Lgort',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sSelectedKey
                });

                var aFilter = [];
                aFilter.push(oFilterWerks, oFilterLgort);

                vDialog.getModel().read("/List_FloorSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Floor = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
                vDialog.getModel().read("/List_StorSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Stor = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
                vDialog.getModel().read("/List_RackSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Rack = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
                vDialog.getModel().read("/List_CellSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Cell = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
            },
            onChangeFloor: function (oEvent) {
                oComboBoxFloor = oEvent.getSource();
                var sSelectedKey = oComboBoxFloor.getSelectedKey();

                if (!sSelectedKey) {
                    oComboBoxFloor.setValueState(ValueState.Error);
                } else {
                    oComboBoxFloor.setValueState(ValueState.Success);
                }
                var oModel = this._dialog.getModel("selected");
                oModel.setProperty("/stor").clear;
                oModel.setProperty("/rack").clear;
                oModel.setProperty("/cell").clear;
                if(oComboBoxStor)
                {
                    oComboBoxStor.setValueState(ValueState.Error);
                }
                if(oComboBoxRack)
                {
                    oComboBoxRack.setValueState(ValueState.Error);
                }
                if(oComboBoxCell)
                {
                    oComboBoxCell.setValueState(ValueState.Error);
                }

                oFilterFloor = new sap.ui.model.Filter(
                {
                    path: 'Floor',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sSelectedKey
                });

                var aFilter = [];
                aFilter.push(oFilterWerks, oFilterLgort, oFilterFloor);

                vDialog.getModel().read("/List_StorSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Stor = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
                vDialog.getModel().read("/List_RackSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Rack = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
                vDialog.getModel().read("/List_CellSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Cell = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
            },
            onChangeStor: function (oEvent) {
                oComboBoxStor = oEvent.getSource();
                var sSelectedKey = oComboBoxStor.getSelectedKey();

                if (!sSelectedKey) {
                    oComboBoxStor.setValueState(ValueState.Error);
                } else {
                    oComboBoxStor.setValueState(ValueState.Success);
                }
                var oModel = this._dialog.getModel("selected");
                oModel.setProperty("/rack").clear;
                oModel.setProperty("/cell").clear;
                if(oComboBoxRack)
                {
                    oComboBoxRack.setValueState(ValueState.Error);
                }
                if(oComboBoxCell)
                {
                    oComboBoxCell.setValueState(ValueState.Error);
                }

                oFilterStor = new sap.ui.model.Filter(
                {
                    path: 'Stor',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sSelectedKey
                });

                var aFilter = [];
                aFilter.push(oFilterWerks, oFilterLgort, oFilterFloor, oFilterStor);

                vDialog.getModel().read("/List_RackSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Rack = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
                vDialog.getModel().read("/List_CellSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Cell = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
            },
            onChangeRack: function (oEvent) {
                oComboBoxRack = oEvent.getSource();
                var sSelectedKey = oComboBoxRack.getSelectedKey();

                if (!sSelectedKey) {
                    oComboBoxRack.setValueState(ValueState.Error);
                } else {
                    oComboBoxRack.setValueState(ValueState.Success);
                }
                var oModel = this._dialog.getModel("selected");
                oModel.setProperty("/cell").clear;
                if(oComboBoxCell)
                {
                    oComboBoxCell.setValueState(ValueState.Error);
                }

                oFilterRack = new sap.ui.model.Filter(
                {
                    path: 'Rack',
                    operator: sap.ui.model.FilterOperator.EQ,
                    value1: sSelectedKey
                });

                var aFilter = [];
                aFilter.push(oFilterWerks, oFilterLgort, oFilterFloor, oFilterStor, oFilterRack);

                vDialog.getModel().read("/List_CellSet", {
                    filters: aFilter,
                    success: function(oEvt) {
                    //обрабатываем положительный ответ
                        tableListBox.Cell = oEvt.results;
                        vDialog.getModel("Table").setProperty('/', tableListBox);
                    },
                    error: function(oEvt) {
                    //обрабатываем ошибку

                    }
                });
            },
            onChangeCell: function (oEvent) {
                oComboBoxCell = oEvent.getSource();
                var sSelectedKey = oComboBoxCell.getSelectedKey();

                if (!sSelectedKey) {
                    oComboBoxCell.setValueState(ValueState.Error);
                } else {
                    oComboBoxCell.setValueState(ValueState.Success);
                }
            },
            onChangeType: function (oEvent) {
                var oValidatedComboBox = oEvent.getSource(),
                    sSelectedKey = oValidatedComboBox.getSelectedKey();

                if (!sSelectedKey) {
                    oValidatedComboBox.setValueState(ValueState.Error);
                } else {
                    oValidatedComboBox.setValueState(ValueState.Success);
                }
            }
        }
    }
)